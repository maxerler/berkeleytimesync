package ca.ubc.berkeley;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ca.ubc.berkeley.Message.MessageType;

/**
 * Master process. Responsible for implementation of the
 * Berkeley Algorithm. Polls slave processes for their time,
 * computes the average time for all processes, and sends out
 * the differences between each slave's time and the average time.
 * The process also adjusts its own clock.
 *
 * @author max
 *
 */
public class Master extends Process {
	private List<Integer> slaves;

	/**
	 * Constructor. Simply assigns the pid of the process
	 * and starts its clock.
	 *
	 * @param pid
	 */
	public Master(int pid) {
		super(pid);
	}

	/**
	 * Sets the list of slaves. Each entry in the list
	 * is the port the slave process will listen on.
	 *
	 * @param slaves
	 */
	public void setSlaves(List<Integer> slaves) {
		this.slaves = slaves;
	}

	/**
	 * Polls all slave processes for their clock times and
	 * returns them in list form. The returned list includes
	 * the time of the master process clock as the last element.
	 * Times from slave processes are adjusted by half the round
	 * trip time of the message send and receive.
	 * 
	 * @return
	 */
	private List<Long> getProcessTimes() {
		List<Future<Long>> futureTimes = new ArrayList<Future<Long>>();
		List<Long> times = new ArrayList<Long>();
		ExecutorService exec = Executors.newCachedThreadPool();

		for (final int slave : slaves) {
			futureTimes.add(exec.submit(new Callable<Long>() {

				@Override
				public Long call() throws Exception {
					try {
						Socket soc = new Socket("localhost", slave);

						long start = System.currentTimeMillis();
						ObjectOutputStream out = new ObjectOutputStream(soc.getOutputStream());
						out.writeObject(new Message(MessageType.TIME_REQUEST, null, null));
						ObjectInputStream in = new ObjectInputStream(soc.getInputStream());
						Message m = (Message) in.readObject();
						long stop = System.currentTimeMillis();

						out.close();
						in.close();
						soc.close();

						long rtt = stop - start;
						return m.getTime() + (rtt/2);

					} catch (IOException | ClassNotFoundException e) {
						e.printStackTrace();
						return null;
					}
				}

			}));
		}

		Long masterTime = clock.getTime();

		for (Future<Long> futureTime : futureTimes) {
			try {
				Long time = futureTime.get();
				times.add(time);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				times.add(null);
			}
		}
		times.add(masterTime);
		exec.shutdown();
		return times;
	}

	/**
	 * Computes the average of a list of Longs. Null
	 * values are not considered in the average.
	 *
	 * @param times
	 * @return
	 */
	private long getAverageTime(List<Long> times) {
		long sum = 0;
		int n = 0;
		for (Long time : times) {
			if (time != null) {
				sum += time;
				n++;
			}
		}
		return sum/n;
	}

	/**
	 * Computes the differences between each time in times and
	 * the average time and returns the differences in list form.
	 *
	 * @param times
	 * @param averageTime
	 * @return
	 */
	private List<Long> getTimeDiffs(List<Long> times, Long averageTime) {
		List<Long> diffs = new ArrayList<Long>();
		for (Long time : times) {
			diffs.add(time == null ? null : averageTime - time);
		}
		return diffs;
	}

	/**
	 * Sends the respective time differences of each slave
	 * process. Slave processes then adjust their own clocks
	 * with these delta values.
	 *
	 * @param diffs
	 */
	private void syncSlaves(List<Long> diffs) {
		for (int i = 0; i < slaves.size(); i++) {
			try {
				Long delta = diffs.get(i);
				if (delta != null) {
					int slave = slaves.get(i);
					Socket soc = new Socket("localhost", slave);
					ObjectOutputStream out = new ObjectOutputStream(soc.getOutputStream());
					out.writeObject(new Message(MessageType.DELTA, null, diffs.get(i)));
					out.close();
					soc.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Performs the Berkely time synchronization algorithm.
	 *
	 * @param iteration
	 */
	private void syncTime(int iteration) {
		List<Long> times = getProcessTimes();
		long averageTime = getAverageTime(times);
		List<Long> diffs = getTimeDiffs(times, averageTime);
		clock.adjustTime(diffs.get(diffs.size() - 1));
		System.out.println("Time Synchronization iteration " + iteration + ":");
		System.out.println("    Set clock of master with pid " + pid +" to: " + clock.getTime());
		syncSlaves(diffs);
	}

	/**
	 * Run method. Syncs the time and then sleeps for ten seconds.
	 */
	@Override
	public void run() {
		int i = 1;
		while (true) {
			syncTime(i);
			BerkeleyTimeSync.completedIteration();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i++;
		}
	}
}
