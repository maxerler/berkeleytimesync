package ca.ubc.berkeley;

import java.io.Serializable;

/**
 * A class representing a message to be passed between master and
 * slave processes. The message can be a time request from the master
 * to a slave, a time response from a slave to the master, or a delta
 * value from the master to a slave.
 *
 * @author max
 *
 */
public class Message implements Serializable {

	public static enum MessageType {
		TIME_REQUEST,
		TIME_RESPONSE,
		DELTA;
	}

	private static final long serialVersionUID = -1099154478386401759L;
	private MessageType type;
	private Long time;
	private Long delta;
	
	public Message(MessageType type, Long time, Long delta) {
		this.type = type;
		this.time = time;
		this.delta = delta;
	}
	
	public MessageType getType() {
		return type;
	}
	
	public Long getTime() {
		return time;
	}
	
	public Long getDelta() {
		return delta;
	}
}
