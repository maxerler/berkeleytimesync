package ca.ubc.berkeley;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Abstract class that Slave and Master extend. Simulates a process
 * with an independent clock.
 *
 * @author max
 *
 */
public abstract class Process implements Runnable {
	/**
	 * A class to simulate a running clock. Intended to be run
	 * in a separate thread so that the time appears to be constantly
	 * ticking.
	 *
	 * @author max
	 *
	 */
	public class Clock implements Runnable {
		private volatile AtomicLong time;
		
		public Clock() {
			time = new AtomicLong(0);
		}
		
		@Override
		public void run() {
			while (true) {
				time.addAndGet(1);
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		public long getTime() {
			return time.get();
		}
		
		public void adjustTime(long delta) {
			time.addAndGet(delta);
		}
	}
	
	protected Clock clock;
	protected int pid;
	
	public Process(int pid) {
		this.pid = pid;
		this.clock = new Clock();
		new Thread(clock).start();
	}

	@Override
	public abstract void run();
}
