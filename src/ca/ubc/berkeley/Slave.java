package ca.ubc.berkeley;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import ca.ubc.berkeley.Message.MessageType;

/**
 * A class representing a slave process. Loops receiving
 * messages from the master process and responding, adjusting
 * its clock as required by the master.
 * 
 * @author max
 *
 */
public class Slave extends Process {
	private ServerSocket ssoc;
	
	/**
	 * Constructor. Opens a server socket to listen for messages
	 * from the master process and starts its clock.
	 * 
	 * @param pid
	 */
	public Slave(int pid) {
		super(pid);
		try {
			ssoc = new ServerSocket(pid);
		} catch (IOException e) {
			System.out.println("Error creating server socket on port " + pid + ": " + e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * Handles a message from the master process. If the message is a
	 * time request, then this process sends its clock's current time.
	 * If the message is a delta message, then this process adjusts its
	 * clocks time by the given delta value.
	 * 
	 * @param m
	 * @param out
	 * @throws IOException
	 */
	private void handleMessage(Message m, ObjectOutputStream out) throws IOException {
		switch (m.getType()) {
		case TIME_REQUEST:
			out.writeObject(new Message(MessageType.TIME_RESPONSE, clock.getTime(), null));
			break;
		case DELTA:
			clock.adjustTime(m.getDelta());
			System.out.println("    Set clock of process " + pid +" to: " + clock.getTime());
			break;
		}
	}
	
	/**
	 * Loops endlessly receiving messages from the master process
	 * and handling them.
	 */
	@Override
	public void run() {
		while (true) {
			try {
				Socket soc = ssoc.accept();
				ObjectInputStream in = new ObjectInputStream(soc.getInputStream());
				Message m = (Message) in.readObject();
				ObjectOutputStream out = new ObjectOutputStream(soc.getOutputStream());
				handleMessage(m, out);
				in.close();
				out.close();
				soc.close();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
	}

}
