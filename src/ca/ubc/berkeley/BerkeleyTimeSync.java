package ca.ubc.berkeley;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * A program simulating the Berkeley time synchronization algorithm.
 * Program argument is the number of slave processes to spawn. Each
 * process (including a master process) has its own "clock," which is
 * simulated by each process spawning a "clock" thread that increments
 * a counter. For more information on the algorithm, check the link below.
 * 
 * @author max
 * @see <a href="http://en.wikipedia.org/wiki/Berkeley_algorithm">Berkeley Algorithm</a>
 *
 */
public class BerkeleyTimeSync {
	private static final int MAX_PID = 20000;
	private static final int MIN_PID = 10000;
	private static Random r = new Random();
	private static Set<Integer> USED_PIDS = new HashSet<Integer>();
	private static CountDownLatch LATCH;

	/**
	 * Main method. Spawns master and slave threads which enact a 
	 * simulation of the Berkeley Algorithm.
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		if (args.length != 2) {
			System.out.println("Please supply the number of slave processes and the number of iterations to run.");
			System.out.println("Usage: java BerkeleyTimeSync <number of processes> <number of iterations>");
			return;
		}

		int numSlaves = 0;
		int iterations = 0;

		try {
			numSlaves = Integer.parseInt(args[0]);
			iterations = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.out.println("Please supply the number of slave processes and the number of iterations to run.");
			System.out.println("Usage: java BerkeleyTimeSync <number of processes> <number of iterations>");
			return;
		}

		if (numSlaves < 0) {
			System.out.println("Please run this program with at least 1 slave process and at least 1 iteration.");
			System.out.println("Usage: java BerkeleyTimeSync <number of processes> <number of iterations>");
			return;
		}

		LATCH = new CountDownLatch(iterations);

		List<Integer> slaves = new ArrayList<Integer>();
		for (int i = 0; i < numSlaves; i++) {
			int pid = getRandPid(MIN_PID, MAX_PID);
			slaves.add(pid);
			new Thread(new Slave(pid)).start();
		}

		Master master = new Master(getRandPid(MIN_PID, MAX_PID));
		master.setSlaves(slaves);
		new Thread(master).start();
		awaitCompletion();
		System.exit(0);
	}

	/**
	 * Produces a random, unique number between min and max,
	 * inclusive. Repeated calls to this method will never
	 * return the same number, provided the method is called
	 * fewer than (max - min + 1) times. Calling this method
	 * more than (max - min + 1) times with the same max and 
	 * min arguments will result in an infinite loop.
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	private static int getRandPid(int min, int max) {
		int num = r.nextInt((max - min) + 1) + min;
		while (USED_PIDS.contains(num)) {
			num = r.nextInt((max - min) + 1) + min;
		}
		USED_PIDS.add(num);
		return num;
	}

	/**
	 * Waits for the master and slave processes to complete the provided
	 * number of iterations.
	 */
	private static void awaitCompletion() {
		try {
			LATCH.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Lets the main thread know that one iteration has completed. This
	 * method should be called by the master process after each completed
	 * time synchronization.
	 */
	public static void completedIteration() {
		LATCH.countDown();
	}
}
